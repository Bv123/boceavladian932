package com.example.acer_pc.androidlab;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

public class BookDetailActivity extends AppCompatActivity {
    private int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        Intent i=getIntent();
        String Author=i.getStringExtra("author");
        String Name=i.getStringExtra("name");
        id=i.getIntExtra("id",-1);
        final EditText EditName=(EditText)findViewById(R.id.editText4);
        EditText EditAuthor=(EditText)findViewById(R.id.editText5);
        EditAuthor.setText(Name);
        EditName.setText(Author);
    }
    @Override
    public void finish(){
        final EditText EditName=(EditText)findViewById(R.id.editText4);
        EditText EditAuthor=(EditText)findViewById(R.id.editText5);
        Intent data=getIntent();
        data.putExtra("name",EditName.getText().toString());
        data.putExtra("author",EditAuthor.getText().toString());
        data.putExtra("id",id);
        setResult(RESULT_OK,data);
        super.finish();
    }
}
