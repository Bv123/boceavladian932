/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  ListView,
  Navigator,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Linking,
} from 'react-native';
import Realm from 'realm';
import PopupDialog, { SlideAnimation } from 'react-native-popup-dialog';
import Chart from 'react-native-chart';



let realm = new Realm({
  		schema: [{name: 'Book', primaryKey: 'id',properties: {author:'string', name: 'string', id: 'string', nr:'int', index:'int'}}]
});

const data = [
    [0, 1],
    [1, 3],
    [3, 7],
    [4, 9],
];
class SimpleChart extends React.Component {
    render() {
        return (
            <View style={styles.chartContainer}>
                <Chart
                    style={styles.chart}
                    data={data}
                    type="bar"
                    showDataPoint={true}
                    showGrid={false}
                 />
            </View>
        );
    }
}

class ChartButton extends React.Component{
    constructor(props){
        super(props);
        this.state={
            booksList : props.booksList,
        }
    }
    render(){
        return(
            <View>
                <TouchableHighlight onPress = {
                           ()=> {this.props.navigator.push({ index : 2 , passProps : {}});
                           }}
            >
                     <View>
                          <Text>Chart!</Text>
                     </View>
                </TouchableHighlight>
            </View>
        )
    }
}

class AddScene extends React.Component{
    constructor(props){
        super(props);
        this.state={
            update : props.update,
        }
    }
    render(){
        return(
            <View>
            <TouchableHighlight onPress={() => {this.popupDialog.openDialog();}}>
                                             <View>
                                                  <Text>Add Dialog</Text>
                                             </View>
            </TouchableHighlight>
             <PopupDialog ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                   dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
             >
                       <View>
                             <Text>Add a book</Text>
                             <Text> Id : </Text><TextInput onChangeText={(text) => this.state.id =  text}/>
                             <Text> Name : </Text><TextInput onChangeText={(text) => this.state.name =  text}/>
                             <Text> Author : </Text><TextInput onChangeText={(text) => this.state.author =  text}/>
                             <TouchableHighlight onPress={() => {realm.write(() => {
                                 realm.create('Book', {id: this.state.id, name: this.state.name,author:this.state.author, nr: 35, index:1});});
                                 this.props.update();
                                 }}>
                                 <View>
                                     <Text>Add!</Text>
                                 </View>
                             </TouchableHighlight>
                       </View>
             </PopupDialog>
             </View>
        )
    }
}

class DeleteScene extends React.Component{
    constructor(props){
        super(props);
        this.state={
            update : props.update,
        }
    }
    render(){
        return(
            <View>
            <TouchableHighlight onPress={() => {this.popupDialog.openDialog();}}>
                                             <View>
                                                  <Text>Delete Dialog</Text>
                                             </View>
            </TouchableHighlight>
             <PopupDialog ref={(popupDialog) => { this.popupDialog = popupDialog; }}
                   dialogAnimation = { new SlideAnimation({ slideFrom: 'bottom' }) }
             >
                       <View>
                             <Text>Delete a book</Text>
                             <Text> Id : </Text><TextInput onChangeText={(text) => this.state.id =  text}/>
                             <TouchableHighlight onPress={() => {realm.write(() => {
                                 var book = realm.objects('Book').filtered('id ==$0', this.state.id);
                                 realm.delete(book);
                                 this.props.update();
                                 })}
                                 }>
                                 <View>
                                     <Text>Delete!</Text>
                                 </View>
                             </TouchableHighlight>
                       </View>
             </PopupDialog>
             </View>
        )
    }
}

const routes = [
    {
        title : "BookListScene",
        index : 0
    },
    {
        title : "BookScene",
        index : 1
    },
    {
        title : "ChartScene",
        index : 2
    }
]

class EditScene extends React.Component{
    constructor(props){
        super(props);
        this.state={
            id : props.id,
        }
    }
    updateName= (text) => {
            var books = realm.objects('Book').filtered('id ==$0', this.state.id);
            var book = books[0];
        realm.write(() => {
            book.name=text;
          })
    }
     updateAuthor= (text) => {
                      var books = realm.objects('Book').filtered('id ==$0', this.state.id);
                      var book = books[0];
                  realm.write(() => {
                      book.author=text;
                    })
    }
    render(){
        return (
            <View style={styles.listContainer}>
                <Text> Edit name : </Text>
                      <TextInput
                        onChangeText ={(text) => {this.updateName(text);this.props.update()}}
                      />
                      <Text> Edit author : </Text>
                    <TextInput
                               onChangeText ={(text) => {this.updateAuthor(text);this.props.update()}}
                       />
            </View>
        );
    }
}

class BookList extends React.Component{
    updateDs = () => {
        books = realm.objects('Book');
        this.setState({
           dataSource: this.data.cloneWithRows(books)
         })
   }
    constructor(props){
        super(props);
        this.data = new ListView.DataSource({rowHasChanged:(r1,r2)=> r1 !== r2});
        books = realm.objects('Book');
        this.state= {
            update : this.updateDs,
            booksList : books,
            dataSource: this.data.cloneWithRows(books)
        };
    }

    render(){
        return(
        <View>
            <ListView style={styles.listContainer}
                dataSource={this.state.dataSource}
                renderRow={(rowData)=>
                        <TouchableOpacity onPress = {
                           ()=> {this.props.navigator.push({ index : 1 , passProps : {
                                name : rowData.name,
                                author:rowData.author,
                                id : rowData.id,
                                update : this.updateDs
                           }});
                               Linking.openURL('mailto:boceadacian@gmail.com?subject=OriginalValues&body='+"name : "+rowData.name + " id : " + rowData.id)
                           }}
                        >
                            <View>
                                <Text> Book name : { rowData.name } Book author : { rowData.author} Book id : { rowData.id } </Text>
                            </View>
                        </TouchableOpacity>
                }
                renderSeparator = {
                    (sectionID, rowID, adjacentRowHighlighted) => <View key={rowID} style = {{height:1, backgroundColor : 'lightgray'}} />
                }
            />
            <AddScene {...this.state}/>
            <DeleteScene {...this.state}/>
            <ChartButton  navigator={this.props.navigator} route={routes[2]} {...this.state}/>
        </View>
        );
    }
}



export default class App extends Component {
    constructor(props){
        super(props);
    this.state = {
      name: '',
      id: 0,
    }
    }
  render() {
    return (
      <View style={styles.container}>
        <Navigator
          initialRoute={routes[0]}
          initialRouteStack={routes}
          renderScene={
            (route, navigator) => {
              switch (route.index) {
                case 0 : return (
                <View>
                    <BookList navigator={navigator} route={routes[route.index]} {...route.passProps}> </BookList>
                </View>);
                case 1 : return (<EditScene navigator={navigator} route={routes[route.index]} {...route.passProps}> </EditScene>);
                case 2 : return (<SimpleChart navigator={navigator} route={routes[route.index]} {...route.passProps}></SimpleChart>);
              }
            }
          }
          configureScene={
            (route, routeStack) =>
              Navigator.SceneConfigs.FloatFromBottom
          }
          navigationBar={
           <Navigator.NavigationBar
             routeMapper={{
               LeftButton: (route, navigator, index, navState) => {
                 if (route.index == 0){
                   return null;
                 }
                 return (
                   <TouchableHighlight onPress={()=>navigator.pop()}>
                     <Text>Back</Text>
                   </TouchableHighlight>
                 )
               },
               RightButton: (route, navigator, index, navState) => { return null; },
               Title: (route, navigator, index, navState) =>
                 { return (<Text> {routes[route.index].title} </Text>); },
             }}
             style={styles.navigationBar}
           />
        }
      />
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  listContainer: {
    paddingTop: 60
  },
  navigationBar:{
    backgroundColor: 'green',
  },
  chartContainer: {
    flex: 1,
    paddingTop: 85,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  chart: {
    width: 200,
    height: 200,
  }
});

AppRegistry.registerComponent('AwesomeProject', () => App);